# Compiling proceedings for upload to HAL

[HAL](https://hal.science) is an open archive which can be accessed through various interfaces, including two web interfaces (respectively [hal.science](https://hal.science) and [x2hal.inria.fr](https://x2hal.inria.fr/)) for manual deposits, and a [SWORD API](https://api.archives-ouvertes.fr/sword/upload/) interface for programmatic deposits (all these interfaces require users to have a HAL account, which can be created for free at [hal.science](https://hal.science/user/create)).

Setting **taln2x** options to compile proceedings for HAL upload amounts to add the following to ==config.toml==:

<pre>
out_format   = "hal"            # Output format
</pre>

Additional options can be defined:

<pre>
[project.hal]
x_onbehalf   = ""              # Declare deposit owners
stamp        = "TALN-RECITAL"  # Declare HAL collections
include_pdf  = true            # Include PDF in ingestion
guess        = false           # Let HAL guess affiliations
dry_run      = false           # Test option on HAL server
halid        = false           # Insert hal identifiers in 
                               # papers (see tex template)
update       = true            # Update existing references
national     = false           # Declare event to be national
no_meta      = false           # Do not update metadata
</pre>

These are commented below.

## Overview of the process

Two types of deposits are available on HAL: bibliographical notices and documents (notices *with* pdf files). A major difference between these two types is that notices are available directly, while documents need to go through a manual validation from HAL's maintainers before being visible.

Depending on which type of deposit one wants **taln2x** to prepare, two scenarios can happen as described below.

### Uploading notices

When uploading notices, **taln2x** will compile an XML file per article (plus an XML file per volume) following [HAL's XML schema](https://api.archives-ouvertes.fr/documents/all.xml). On top of these, **taln2x** will compile one bash script per article with the adequate API request's headers (see [documentation](https://api.archives-ouvertes.fr/docs/sword/)).

For convenience, a wrapper bash script per volume is also created at the root of the output directory, by running this file, one automatically runs all papers' scripts.

### Uploading documents

When uploading documents, **taln2x** will add to the above mentioned XML files the PDF version of articles, both XML and PDF files will be gathered in a ZIP file, which will be uploaded to HAL, as described [here](https://github.com/CCSDForge/HAL/raw/master/Sword/SWORD_import_HAL.pdf) or illustrated in [this example](https://api.archives-ouvertes.fr/documents/depot.zip).

Once the output files are compiled by **taln2x**, one can export two environment variables named *LOGIN* and *PASSWORD* containing HAL's credentials, and then eventually bash wrappers.

Log files will be created to store the pieces of information sent back by HAL API.
