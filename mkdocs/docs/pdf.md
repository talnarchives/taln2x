# Compiling full proceedings in PDF

In order to compile full proceedings in PDF format, one needs to set up **taln2x** options in ==config.toml== as follows (in `[project.core]`):

<pre>
out_format   = "pdf"            # Output format
</pre>

## Overview of the process

Basically, when creating a new project **taln2x** copies a set of files from its install directory to the newly created project directory. These files are used when compiling pdf proceedings. They include three latex templates (named `pre-proceedings.tex`, `pre-proceedings-en.tex` and `single_paper.tex`) and three image files (named `by.eps`, `logo.png` and `background.png`).

These correspond respectively to the template for full proceedings in French, the template for full proceedings in English (e.g. for date format), the template for papers (so that a custom header and footer can be added to them), the logo of the CC-BY license (which is the default license displayed in papers' footer), the logo of the event (default being the [ATALA](https://atala.org) logo) and the background image appearing in the frontpage of the proceedings (default being a stained-glass window used in the [TALN 2020 proceedings](https://aclanthology.org/2020.jeptalnrecital-jep.0.pdf)).

> **Please note that you can adapt these files to your needs prior to invoking `taln2x build`.**

Please note also that you can optionally add, within each track directory, the following files:

- `sponsors.png` (image containing the sponsors' logos)
- `credits.tex` (tex string containing credit information)
- `message.tex` (tex string containing a message for the readers)
- `preface.tex` (tex string containing the preface of the volume)
- `committee.tex` (tex string containing the committees)
- `invited.tex` (tex string containing invited talks)
- `isbn.tex` (tex string containing the volume ISBN number)

## Miscellaneous options

In the ==config.toml== config file, three other options can be defined:

<pre>
[project.pdf]
tex_log      = false            # Removal of latex logs
img          = ""               # User's image directory
language     = "fr"             # To select latex template
</pre>

If you set `tex_log` to true, during the compilation of the proceedings latex sources, the log files will not be erased.

If you specify a path for the `img` option, it will be used as a directory where to look for logo and background images. This is a legacy feature, one should rather put the image files directly in the project directory.

The language option can be set to either `fr` (French) or `en` (English) and is used to select the adequate template to compile the PDF proceedings.
