# How to use taln2x ?

Basically, as a **taln2x** user, one needs to know the following:

- how to *prepare data and metadata*, which means understanding **taln2x**'s expected *input formats* ;

- how to *configure* **taln2x**, which means setting its options accordingly to the expected output ;

- how to *check that the compiled proceedings* are correct, which means understanding **taln2x** *output format*.

To acquire this knowledge, you can have a look at the [in/out formats](formats.md) and [configuration](options.md) sections.
