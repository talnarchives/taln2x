# Workflow

Basically, **taln2x** works as follows:

**1)** First, it reads the command line options and then loads the config file (==config.toml==) to determine which python modules (and functions) should be used to compile the proceedings (note that CLI has priority over config file)

**2)** Second, it reads the event configuration file (==event.yml==) to get the conference meta-data (title, booktitle, acronym, chairs, tracks, etc.) and instantiate (i) a corresponding `Event` python object together with (ii) the needed `Track` objects (stored in a dictionary which is an attribute of the event).

**3)** Third, it reads the CSV (easychair input), XML (TALN-archive input) or ZIP (sciencesconf input) file provided by the user in order to:

   - get articles' metadata (id, title, authors, abstract, url, etc.) and instantiate corresponding `Article` python objects ;

   - (optionaly) get authors' descriptions (email, affiliation) when available and instantiate corresponding `Author` python objects.

**4)** It writes output data (in `out/`) depending on the required output format:

   - PDF files (and their LaTeX sources),
   - XML files (together with bash scripts containing curl http queries) as required by HAL's webservice [(cf SWORD import API)](https://api.archives-ouvertes.fr/docs/sword/),
   - Bibtex and PDF files required by the ACL anthology [(cf section 2c of the guidelines)](https://acl-org.github.io/ACLPUB/anthology.html),
   - XML-based metadata and pdf files required by the TALN-archives [(cf documentation)](https://github.com/boudinfl/taln-archives),
   - or XML file required by DBLP [(cf DBLP's FAQ)](https://dblp.org/faq/1474621.html).
