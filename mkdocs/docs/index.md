# About

**taln2x** is a [python](https://www.python.org/) program which aims at facilitating the edition and publication of collections of scientific papers (especially workshop or conference proceedings).

**taln2x** is a follow-up of [taln2acl](https://gitlab.com/parmenti/taln2acl), a [Python/OrgMode](https://orgmode.org/) program which has been used to upload the proceedings of the [TALN](https://www.atala.org/-Conference-TALN-RECITAL) (French-speaking NLP) conferences from 2001 to 2022 to the [ACL anthology](https://aclanthology.org) open archives.

# Basic concepts

**taln2x** takes as input a set of scientific papers (together with metadata such as volume names, editors, publication dates ...), which have been collected either manually or else using an [abstract management system](https://en.wikipedia.org/wiki/Abstract_management)[^1], and builds full proceedings either as a set of PDF files (volumes), or as a set of XML and / or Bibtex files depending on the target [open archives](https://en.wikipedia.org/wiki/Open_Archives_Initiative)[^2].

# Basic usage

!['Gif illustration of taln2x command line'](img/taln2x.gif)

# License

**taln2x** is released under the terms of the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.html).

# Credits

**taln2x** has been developed with the help of [Sylvain Pogodalla](https://members.loria.fr/SPogodalla/), on behalf of [ATALA](https://atala.org) (the French Association for Computational Linguistics). It reuses code from Nils Blomqvist's [easy2acl](https://github.com/nblomqvist/easy2acl) and Florian Boudin's [taln-archives](https://github.com/boudinfl/taln-archives).

<!-- The **taln2x** logo has been created using the [Letterblocks fonts](https://www.1001fonts.com/letterblocks-font.html) made by Vladimir Nikolic. -->

# Contact

Comments or questions are welcome, and can be sent to **taln2x**'s maintainer ([Yannick Parmentier](mailto:yannick.parmentier@loria.fr?subject=taln2x)).

[^1]: At the moment **taln2x** supports the [easychair](https://easychair.org) and [sciencesconf](https://www.sciencesconf.org/en/) systems.

[^2]: Currently supported archives are [HAL](https://hal.science), [TALN-archives](http://talnarchives.atala.org), [ACL anthology](https://aclanthology.org/) and [DBLP](https://dblp.org/).