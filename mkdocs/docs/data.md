# Code structure

**taln2x** source code is split in two main modules:

`utils`

: which contains the data type definitions (starting with an uppercase letter) and functions used to read input data (these functions are spread over various modules according to the target input format)

`exports`

: which contains functions used to write output data depending on the target output format

More precisely, **taln2x** sources contain the following files:

<pre>
src/taln2x
├── exports
│   ├── anthology.py
│   ├── archives.py
│   ├── dblp.py
│   ├── hal.py
│   ├── __init__.py
│   └── pdf.py
├── __init__.py
├── __main__.py
├── templates
└── utils
    ├── archives.py
    ├── Article.py
    ├── Author.py
    ├── easychair.py
    ├── Event.py
    ├── __init__.py
    ├── readevent.py
    ├── readpdf.py
    ├── sciencesconf.py
    ├── searchid.py
    ├── taln_archives_parser.py
    └── Track.py
</pre>

**taln2x** main interface is defined in `__main__.py`. Its version number is defined in `__init__.py`. 

# Data types

**taln2x** uses three main data types (defined in the `utils` module):

`Event`

: basically an event corresponds to a conference, which has a title, an abbreviated title, chairs, a venue, a proceedings book title (and short title), begin and end dates, a URL for its website, a publisher, and (at least one) track.

`Track`

: a track consists of an id (which also corresponds to the track directory), seome acceptance keywords, chairs, volume name, authors, articles (which may be grouped into sessions), a start page number, an ordering criterion and a base_url (for online storing).

`Article`

: an article has an identifier (usually given by the conference management system, and which is also part of the paper's PDF filename), a title (a secondary title such as the title in another language), a language (a secondary language), authors, an asbtract, keywords, page numbers, a track (and optionally a session) it belongs to, a URL, and optionally an identifier on HAL.

`Author`

: authors have a firstname, lastname, affiliation, rank (among authors of a given paper), and email.
