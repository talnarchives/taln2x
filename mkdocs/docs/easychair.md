---
tiles:
  - caption: "@easychair"
    img_src: "../img/easychair.png"
    alt_text: 'illustration of easychair interface'
    tooltip:  'Easychair web interface for submission management'
show_tiles_inline: true
---

# Getting data and metadata from easychair

When using [easychair](https://easychair.org) as an abstracts management system, one has access to a web interface to manage a given event (that is, to collect papers, assign reviewers, accept papers, etc.). From this web portal, one can get all necessary pieces of information for **taln2x** to compile proceedings.

Concretely, a few actions are required :

**0.** Create a new **taln2x** project for your event.

**1.** You need to go to the *Submissions* page (using the top menu) and do the following :

  - copy/paste the list of submissions to a file named ==articles.csv== (while making sure that the displayed columns, in terms of order, match the description contained in the `[project.csv]` section of the ==config.toml== configuration file) ;
  
  - click on the *Download submissions* link in the top right menu (highlighted in red in the figure below) to get a zip archive containing the pdf final version of all articles.

{{ tile_grid(page.meta) }}

**2.** Then, you need to get the abstracts of accepted papers by going to *Administration/Other utilities/List of accepted submissions* and then click on the *Show abstracts* and *Download the list* links in the top right menu. You will then get a file named ==accepted.html==.

**3.** Finally, you need to get the authors' descriptions by going to *Administration/Authors/Email addresses* and then click on the *Excel file* link in the top right menu. You will get a file named ==author_list.xlsx==. Unfortunately, the background color used in the spreadsheets contained in this file will cause **taln2x** to crash with the following error message:

<pre>
ValueError: Unable to read workbook: could not read stylesheet from .../author_list.xlsx.
This is most probably because the workbook source files contain some invalid XML.
Please see the exception for more details.
</pre>

To prevent this from happening, you should open ==author_list.xlsx== with e.g. libreoffice, go to the sheet named **All** and remove cells' background color.

Eventually, put these 3 files (==articles.csv==, ==accepted.html== and ==author_list.xlsx==) in the corresponding track directory in your project sources.

**4.** You can then compile your proceedings by (i) setting the adequate options in ==config.toml== (in/out format) and (ii) invoking `taln2x build` from within your project sources.
