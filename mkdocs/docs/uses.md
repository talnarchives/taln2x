# Use cases

In this section, you will find examples of how to use **taln2x** to compile and publish proceedings.

## Using specific input formats

[Getting information from easychair](easychair.md){ .btn .btn-primary .btn-ghost .btn-block }

[Getting information from sciencesconf](sciencesconf.md){ .btn .btn-primary .btn-ghost .btn-block }

## Using specific output formats

[Compiling PDF proceedings](pdf.md){ .btn .btn-primary .btn-ghost .btn-block }

[Compiling proceedings for upload to the ACL anthology](anthology.md){ .btn .btn-primary .btn-ghost .btn-block }

[Compiling proceedings for upload to HAL](hal.md){ .btn .btn-primary .btn-ghost .btn-block }

[Compiling proceedings for upload to the TALN archives](taln.md){ .btn .btn-primary .btn-ghost .btn-block }

[Compiling proceedings for upload to DBPL](dblp.md){ .btn .btn-primary .btn-ghost .btn-block }
