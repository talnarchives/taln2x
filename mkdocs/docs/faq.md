# Frequently Asked Question

Note that questions sent to **taln2x** maintainer will be added to this page.

**-** When I compile my proceedings in HAL format, **taln2x** crashes with the following message, what is going on ?

<pre>
  File "/usr/lib/python3.10/xml/etree/ElementTree.py", line 1045, in _escape_attrib
    if "&" in text:
TypeError: argument of type 'NoneType' is not iterable
</pre>

This error has been observed when metadata associated with a given article contain an author whose country does not belong to [pycountry](https://github.com/flyingcircusio/pycountry)'s database. To fix this, one can reformat the country name in [exports/hal.py](https://gitlab.inria.fr/talnarchives/taln2x/-/blob/main/src/taln2x/exports/hal.py) (see line 302).
