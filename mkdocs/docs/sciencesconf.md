---
tiles:
  - caption: "@sciencesconf1"
    img_src: "../img/sconf_home.png"
    alt_text: 'illustration of sciencesconf interface (home)'
    tooltip:  'Sciencesconf web interface for submission management (home)'
  - caption: "@sciencesconf2"
    img_src: "../img/sconf_export.png"
    alt_text: 'illustration of sciencesconf interface (submissions)'
    tooltip:  'Sciencesconf web interface for submission management (submissions)'
  - caption: "@sciencesconf3"
    img_src: "../img/sconf_type.png"
    alt_text: 'illustration of sciencesconf interface (document types)'
    tooltip:  'Sciencesconf web interface for submission management (document types)'    
  - caption: "@sciencesconf4"
    img_src: "../img/sconf_format.png"
    alt_text: 'illustration of sciencesconf interface (export)'
    tooltip:  'Sciencesconf web interface for submission management (export)'    
show_tiles_inline: true
grid_id: "grid_123"
grid_css: "override_border"
---

<style> 
    .override_border { 
        border: dashed;
        border-width: thin;
    }
    .override_width {
        width: 400px;
    }
    #grid_123 {
        grid-template-columns: 500px;
        grid-row-gap: 1em;
        justify-content: space-evenly;
        justify-items: center;
    }
</style>

# Getting data and metadata from sciencesconf

When using [sciencesconf](https://sciencesconf.org) to manage paper submitted to a conference, one has access to an administration console (see figure @sciencesconf1 below) on the conference website allowing one to export submitted papers and metadata. Via this export functionality, one can get all necessary pieces of information for **taln2x** to compile proceedings.

Concretely, a few actions are required :

**0.** Create a new **taln2x** project for your event.

**1.** Log into your event website on sciencesconf

**2.** Go to *Gestion éditoriale / Dépôts* to access submissions, these can be exported by clicking on the *export* button at the bottom of the page (highlighted in green in figure @sciencesconf2 below)

> Please note that, in order for **taln2x** to properly extract tracks, it is mandatory that tracks were correctly defined in sciencesconf as **document types** before reviewing takes place ! On figure @sciencesconf3 below, one can see sciencesconf's interface for defining document types. **Document types should not contain any special character nor diacritic**.

**3.** Once you clicked on the *export* button, you can define the export options (see figure @sciencesconf4 below). Please make sure to select "Tous les dépôts" (so that you do not have to filter submissions during the previous step), to include metadata and files, and to use tabulation as a separator in the csv file containing papers' metadata.

You will get a file named ==export.zip== which you can save at the root of your project's sources.

**4.** Finally, by setting the required options (in/out format) in ==config.toml== and invoking `taln2x build` you can compile the full proceedings.

{{ tile_grid(page.meta) }}
