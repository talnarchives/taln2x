# How to contribute to **taln2x** ?

To contribute to **taln2x**, you can get its sources from gitlab (see link below), install it locally, commit your changes and submit them via gitlab. 

**taln2**'s sources are versioned using git, and hosted on Inria's gitlab instance.

[Go to taln2x sources on gitlab](https://gitlab.inria.fr/talnarchives/taln2x/){ .btn .btn-primary .btn-ghost .btn-block } 

To install **taln2x** locally from its sources (i.e. without using the version available on [pypi](https://pypi.org)), you can invoke (from **taln2x**'s source directory):

    python3 -m pip install --user -e .

This will install the **taln2x** executable in your home directory, under `.local/bin/`.

