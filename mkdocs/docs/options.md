# About **taln2x** configuration

Configurating a **taln2x** project amounts to specifying two types of information, namely:

- information about the ^^event^^ which underlies the collection of papers (e.g. title, chairs, dates, etc.)

- information about **taln2x** ^^options^^ (e.g. which formats to use)

---

## Event information

Event information is stored in the ==event.yml== file located at the root of the project, and which uses the [yaml](https://yaml.org/) format. The example *event.yml* file created when invoking *taln2x new* illustrates what event information can be used by **taln2x**:[^1]

<pre>
abbrev: MYCONF
title: 19e REncontres MYCONF
url: https://gitlab.inria.fr/talnarchives/taln2x
booktitle: Actes des 19e REncontres MYCONF
short_booktitle: Actes de MYCONF
begin: 2017-06-26
end: 2017-06-30
location: Orléans, France
publisher: ATALA
chairs:
  - UNETELLE, Machine
  - TRUC, Machin
tracks:
  main:
    fullname: "Volume 1 : conférence principale"
    startpage: 1
    order: title
    sessions: long;short;demo
    long: "Papiers longs"
    short: "Papiers courts"
    demo: "Démonstrations"
    accept: ACCEPT_long;ACCEPT_short;ACCEPT_demo
</pre>

Here are the definition (and uses) of these fields:

`abbrev`

: string used as a prefix in article file names (and output directories)

`title`

: string used to refer to the event in output formats 

`url`

: string appearing on PDF proceedings' frontpage

`booktitle`

: string appearing on PDF proceedings' frontpage

`short_booktitle`

: string appearing on PDF proceedings' frontpage

`begin, end`

: dates in ISO-format used in various output formats

`location`

: string used in various output formats

`publisher`

: string appearing on the PDF proceedings

`chairs`

: list of strings of the form "Lastname, Firstname" used in various output formats

`tracks`

: list of dictionaries mapping a track name with attribute-value pairs. At list a track fullname must be given. Information about paper ordering, page numbering or sessions (cf infra) can be given as well.

### tracks

Tracks corresponds to separate ^^volumes^^ in the proceedings. They can have specific chairs (optional field), their page numbering can start from a given integer (optional field). They must have a title (which will be appended to the event title on the PDF proceedings front page). Their papers can be ordered by *title*, *authors* or *input* (which means manually, i.e. following the paper order in the *articles.csv* metadata file).

### sessions

Tracks can also be devided into so-called ^^sessions^^. In that case, papers will be grouped by session, and each session will correspond to a separate part in the PDF proceedings. **This feature is optional**. To use it, one needs:

- to declare which session a given paper belongs to by setting a specific acceptance keyword in ==articles.csv==, a common practice is to suffix the acceptance keyword with the id of the session (as in the example above, where the session ids are *long*, *short* and *demo*).

- to activate **taln2x**'s sessions by setting the *sessions* flag to true in ==config.toml==.

### customization

One can further customize the compiled full proceedings (pdf format) by providing the following files:

`background.png`

: image located in the project root and used in the frontpage of the PDF proceedings (the default picture is a photo of the glasswork "Roses et Mouettes" located in the Maison Bergeret in Nancy, France ; the picture has been taken by Alexandre Prevot, and  is available on Flickr under a CC-BY-SA 2.0 license)

`by.eps`

: image of the license used for each paper (the default license is CC-BY 4.0), located in the project root

`logo.png`

: image used in the frontpage of the PDF proceedings (the default logo is that of the ATALA, the French Association for Natural Language Processing), located in the project root

`sponsors.png`

: an image containing the logos of the sponsoring institutions (optional, located in the track subdirectory)

`credits.tex`

: a LaTeX file containing credits (optional, located in the track subdirectory)

`message.tex`

: a LaTeX file containing a short message to appear before the preface (optional, located in the track subdirectory)

`preface.tex`

: a LaTeX file containing the preface  (optional, located in the track subdirectory)

`committee.tex`

: a LaTeX file containing the organizing / reviewing / etc. committees  (optional, located in the track subdirectory)

`invited.tex`

: a LaTeX file containing a description of the invited talks if any  (optional, located in the track subdirectory)

`isbn.tex`

: a LaTeX file containing the ISBN reference of the proceedings if any  (optional, located in the track subdirectory)

---

## taln2x options

**taln2x** options are set by default by using the ==config.toml== file located at the root of the project. This file uses the [toml](https://toml.io/en/) human-readable format providing mainstream data types (e.g. attribute-value pairs, lists, tables, etc.).

**taln2x**'s options can be overwritten by using the command-line interface. For a full list of options, one can invoke:

    taln2x --help

If you look at ==config.toml==, you will see the full list of **taln2x** options(along with their default values):

<pre>
[project]
name         = "MYCONF"         # Project name

[project.core]
in_format    = "csv"            # Input format
out_format   = "pdf"            # Output format
verbose      = 0                # Verbosity level (0..3)
stopwords    = false            # Ignoring stopwords 
ignore_pdf   = true             # Do not use pdf for metadata
sessions     = true             # Use sessions
latex_encode = false            # Latex encode diacritics
dump         = false            # Dump input config

[project.csv]                   # Input data in CSV
id_col       = 0                # Index of paper id
authors_col  = 1                # Index of authors
title_col    = 2                # Index of title
idhal_col    = 3                # Index of idhal (if any)
file_col     = 4                # Index of file name
accept_col   = 7                # Index of status
keywords_col = 8                # Index of keywords

[project.xml]                   # Input data in XML
xml_file     = ""               

[project.zip]                   # Input data in Zip
zip_file     = ""

[project.pdf]                   # PDF output
tex_log      = false            # Keep latex logs
img          = ""               # User's image directory
language     = "fr"             # Language (cf babel)
halid        = false            # Insert HAL id in paper

[project.acl]                   # ACL output
anthology_id = "myconf"         # Anthology ID 
bilingual    = false            # Bilingual bibtex fields 

[project.hal]                   # HAL output
x_onbehalf   = ""               # Declare deposit owners
stamp        = ""               # Declare HAL collections
include_pdf  = false            # Include PDF 
guess        = false            # Let HAL guess affiliations
dry_run      = false            # Use pre-prod server
update       = false            # Update existing references
national     = false            # Declare national event
no_meta      = false            # Do not update meta-data
</pre>

Note that in the example configuration above, the default csv structure is the one used by [easychair](https://easychair.org). In case you collected metadata using another abstract management system, or manually, you can set the column numbers according to your own csv file.

[^1]: The structure of this *event.yml* file was inspired by the *meta* file used by the [ACL anthology](https://acl-org.github.io/ACLPUB/anthology.html).
