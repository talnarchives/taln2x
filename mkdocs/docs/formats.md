# About taln2x supported formats

The formats used within a given project are defined in ==config.toml== (file located at the root of the project directory). Below is a brief description of the available formats.

---

## Input formats

Currently, **taln2x** supports three formats for storing papers' metadata:

`CSV spreadsheets`

:     The CSV structure (i.e. definitions of columns) is defined in ==config.toml== (the default structure being the one used by the [easychair](https://easychair.org) conference management system).

`XML documents`

:     The XML structure corresponds to that of the [TALN-archives](https://github.com/boudinfl/taln-archives).


`ZIP archives`

:     The structure of these ZIP archives corresponds to the one used by the [sciencesconf](https://doc.sciencesconf.org/gestion-editoriale/) conference management system (basically this archive contains papers' metadata in a file named submissions.csv together with papers' pdf files). 

---

## Output formats

Currently, **taln2x** can build proceedings adhering to any of the following structures:

`PDF`

:    full proceedings having a table of content with hyperlinks and numbered pages (requires `pdflatex`).

`ACL anthology`

:    directory whose structure comply with the [ACL anthology](https://aclanthology.org) [formatting guidelines](https://acl-org.github.io/ACLPUB/anthology.html).

`HAL`

:    (i) set of zip files and bash scripts to upload papers to [HAL](https://hal.science) using [HAL's webservice](https://api.archives-ouvertes.fr/docs/sword) and (ii) bibtex files to alternatively upload papers to HAL using [X2hal](https://doc.archives-ouvertes.fr/x2hal/).

`TALN`

:    directory structure complying with the [TALN-archives](http://talnarchives.atala.org) (such as [this one](https://gitlab.com/parmenti/taln-archives/-/tree/master/TALN/TALN-2020)).

`DBLP`

:    XML file complying with [DBLP](https://dblp.org/)'s [schema](https://dblp.org/xml/subm/dblpsubmission.xsd).


