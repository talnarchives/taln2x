# Compiling proceedings for upload to DBLP

[DBLP](https://dblp.org) is a database containing bibliographical references about papers related to computer science. To upload references to DBLP, one has to encode papers' metadata in an XML format.

In order to compile proceedings in DNLP's XML format, one needs to set up the corresponding option in ==config.toml==:

<pre>
out_format   = "dblp"            # Output format
</pre>

As a result, **taln2x** will compile an XML file complying with [DBLP's XML schema](https://dblp.org/faq/1474621.html).

Please note that this feature has not (yet) been used in practice since adding references to DBLP relies on a decision made by its advisory board, as explained in this [post](https://dblp.org/faq/1474617.html).

